let stringInputOk = 'cdbabacb';
let stringInputError = 'cdbabacccadcb';

let input = [];
let stack = [END_OF_STACK, 'S'];

let debugTable = [];

let analising = true;
let accepted = null;
let iteration = 1;

function cleanGlobals() {
    stack = [END_OF_STACK, 'S'];
    input = [];
    analising = true;
    accepted = null;
    debugTable = [];
    iteration = 1;
}

function analisisState() {
    return {
        input: input.join(''),
        stack: stack.join(''),
        accepted: accepted,
        table: debugTable
    };
}

function randomSentence() {
    let rule = 'S';
    let generating = true;
    let sentence = '';
    while (generating) {
        let ruleLength = grammar[rule].length;
        let production = grammar[rule][Math.floor(Math.random() * ruleLength)];
        if (sentence === '') {
            sentence = production;
        } else {
            sentence = sentence.replace(rule, production);
        }
        let ruleIndex = -1;
        for (let i = 0; i < sentence.length; i++) {
            ruleIndex = NON_TERMINALS.indexOf(sentence[i]);
            if (ruleIndex !== -1) {
                rule = NON_TERMINALS[ruleIndex];
                break;
            }
        }
        if (ruleIndex === -1) {
            generating = false;
        }
    }
    sentence = sentence.replace(EPSILON, '');
    if (sentence.indexOf(EPSILON) !== -1) {
        sentence = sentence.replace(EPSILON, '');
    }
    return sentence;
}

function makeStep() {
    let debugRow = {
        iter: iteration,
        stack: stack.join(''),
        input: input.join('')
    };

    let topStack = stack[stack.length - 1];

    // simbolo atual na entrada
    let inSimbol = input[0];

    // se o topo for o final da pilha e o simbolo de entrada também, foi aceito
    if (topStack === END_OF_STACK && inSimbol === END_OF_STACK) {
        analising = false;
        accepted = true;
        debugRow.action = 'Aceito em ' + iteration + ' iterações';
    } else {
        // se o topo da pilha for igual ao simbolo da entrada, lê a entrada
        if (topStack === inSimbol) {
            debugRow.action = 'Lê \'' + inSimbol + '\'';
            stack.pop();
            input.shift();

            // se existir uma entrada equivalente ao simbolo de entrada ao 
            // não-terminal no topo da pilha na tabela de Parsing
        } else if (
            parsingTable[topStack] !== undefined &&
            parsingTable[topStack][inSimbol] !== undefined
        ) {
            // produção em array da tabela de parsing para o simbolo terminal da entrada
            let toStack = parsingTable[topStack][inSimbol];
            // produção em formato de string
            let production = toStack.join('');

            // adiciona a ação atual na tabela de derivação
            debugRow.action = topStack + ' -> ' + production;

            // remove o topo da pilha
            stack.pop();

            // se a produção não for vazia (epsilon), coloca seu conteúdo da pilha
            if (production !== EPSILON) {
                for (let j = toStack.length - 1; j >= 0; j--) {
                    stack.push(toStack[j])
                }
            }

            // se a análise não for válida, finaliza a mesma com erro
        } else {
            analising = false;
            accepted = false;
            debugRow.action = 'Erro em ' + iteration + ' iterações';
        }
    }
    // incrementa a iteração e coloca a linha gerada na tabela de derivação
    iteration++;
    debugTable.push(debugRow);
}

function oneStepAnalisis(inputString) {
    cleanGlobals();

    input = (inputString + '$').split('');

    while (analising) {
        makeStep();
    }

    return analisisState();
}

let savedInput = '';

function stepByStepAnalisis(inputString) {
    if (inputString !== savedInput || !analising) {
        cleanGlobals();
        savedInput = inputString;
        input = (inputString + '$').split('');
    }

    makeStep();

    return analisisState();
}

function writeDebugTable(table) {
    if (table === undefined) {
        table = debugTable;
    }

    $htmlTable = $('.debug-table > tbody');
    $htmlTable.html('');

    for (let i = 0; i < table.length; i++) {
        $row = $('<tr>');
        $row.append('<td>' + table[i].iter + '</td>');
        $row.append('<td>' + table[i].stack + '</td>');
        $row.append('<td>' + table[i].input + '</td>');
        $row.append('<td>' + table[i].action + '</td>');
        $htmlTable.append($row);
    }
}

function acceptanceFeedback(accepted) {
    $('#input-sentence').removeClass('is-invalid is-valid');

    if (accepted === true) {
        $('#input-sentence').addClass('is-valid');
    } else if (accepted === false) {
        $('#input-sentence').addClass('is-invalid');
    }
}

function updateView(analisis) {
    if (analisis === undefined) {
        acceptanceFeedback(null);
        writeDebugTable([]);
    } else {
        acceptanceFeedback(analisis.accepted);
        writeDebugTable(analisis.table);
    }
}