$(document).ready(function () {
    $('#input-sentence').on('keyup', function () {
        cleanGlobals();
        updateView();
    });

    $('#btn-clean').click(function () {
        $('#input-sentence').val('').focus();
        cleanGlobals();
        updateView();
    });

    $('#btn-verify-sentence').click(function () {
        var analisis = oneStepAnalisis($('#input-sentence').val());
        updateView(analisis);
    });

    $('#btn-verify-step').click(function () {
        var analisis = stepByStepAnalisis($('#input-sentence').val());
        updateView(analisis);
    });

    $('#btn-generate').click(function () {
        $('#btn-clean').click();
        var sentence = randomSentence();
        $('#input-sentence').val(sentence);
    });
});